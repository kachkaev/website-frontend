const productionConfig = {
  publicRuntimeConfig: {
    hostEn: process.env.HOST_EN,
    hostRu: process.env.HOST_RU,
  },
};

module.exports = (phase, rest) =>
  phase === require("next/constants").PHASE_PRODUCTION_SERVER
    ? productionConfig
    : require("next-compose-plugins")(
        [
          require("@next/bundle-analyzer")({
            enabled: process.env.ANALYZE === "true",
          }),
        ],
        {
          ...productionConfig,
          webpack: (config, { isServer }) => {
            const LodashModuleReplacementPlugin = require("lodash-webpack-plugin");
            config.plugins.push(
              new LodashModuleReplacementPlugin({ paths: true }),
            );
            if (!isServer) {
              config.externals = {
                "i18next-browser-languagedetector": "{}",
                "i18next-express-middleware": "{}",
                "./config": "{}",
              };
            }

            return config;
          },
        },
      )(phase, rest);
