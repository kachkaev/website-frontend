FROM node:14.17.6-alpine

RUN mkdir -p /app
WORKDIR /app

COPY package.json yarn.lock /app/
RUN YARN_CACHE_FOLDER=/dev/shm/yarn_cache yarn --production

COPY .next /app/.next
COPY next.config.js /app/next.config.js
COPY build /app/build
COPY public /app/public

EXPOSE 3000

USER node

CMD [ "yarn", "start" ]
