import { css } from "styled-components";

export const base = css`
  a {
    text-decoration: none;
    color: rgb(8, 100, 210);
    border-bottom: 1px solid !important;
    border-bottom-color: rgba(8, 100, 210, 0.3) !important;
  }
  a:visited {
    color: rgb(85, 26, 139);
    border-bottom-color: rgba(85, 26, 139, 0.3) !important;
  }
  a:hover {
    color: #ff0000;
    border-bottom-color: rgba(255, 0, 0, 0.3) !important;
  }

  html {
    height: 100%;
    width: 100%;
  }

  body {
    height: 100%;
    width: 100%;
    overflow-y: scroll;
    position: relative;
    font: 14px/20px Helvetica, Arial, "Lucida Grande", sans-serif;
    text-align: center;

    @media (max-width: 440px) {
      font-size: 13px;
    }

    @media (max-width: 370px) {
      font-size: 12px;
    }
  }

  #__next {
    width: 100%;
    height: 100%;
    position: relative;
  }
`;
