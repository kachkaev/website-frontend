import styled from "styled-components";

const Explanation = styled.div`
  text-align: left;
  max-width: 480px;
  margin: auto;
  padding-top: 0.5em;
  padding-bottom: 1em;
`;

export default Explanation;
