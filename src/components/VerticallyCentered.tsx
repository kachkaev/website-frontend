import React, { FunctionComponent } from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  display: table-cell;
  width: 100%;
  height: 100%;
  padding: 30px 20px;
  vertical-align: middle;
`;

const VerticallyCentered: FunctionComponent = ({ children }) => {
  return <Wrapper>{children}</Wrapper>;
};

export default VerticallyCentered;
